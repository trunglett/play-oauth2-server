package helpers

import scala.util.Random
import java.security.MessageDigest

object Generator {
  def secret: String = MessageDigest.getInstance("md5").digest(Random.nextString(10).getBytes).mkString

  def token: String = secret
}
