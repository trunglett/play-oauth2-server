package helpers

import java.sql.Timestamp
import java.text.SimpleDateFormat

import play.api.libs.json.{Format, JsResult, JsString, JsSuccess, JsValue}

object JsonFormatters {
  implicit def timestampFormat: Format[Timestamp] = new Format[Timestamp] {
    val format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS'Z'")
    def reads(json: JsValue): JsResult[Timestamp] = {
      val str = json.as[String]
      JsSuccess(new Timestamp(format.parse(str).getTime))
    }
    def writes(ts: Timestamp): JsValue = JsString(format.format(ts))
  }
}
