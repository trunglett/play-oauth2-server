package forms


object ClientForm {

  import play.api.data._
  import play.api.data.Forms._

  case class ClientData(redirectUri: Option[String], scope: Option[String])

  val form: Form[ClientData] = Form(
    mapping(
      "redirect_uri" -> optional(text),
      "scope" -> optional(text)
    )(ClientData.apply)(ClientData.unapply)
  )
}
