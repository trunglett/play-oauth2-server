package forms

object UserForm {

  import play.api.data.Forms._
  import play.api.data._

  case class UserData(email: String, password: String)

  val form: Form[UserData] = Form(
    mapping(
      "email" -> text,
      "password" -> text
    )(UserData.apply)(UserData.unapply)
  )
}
