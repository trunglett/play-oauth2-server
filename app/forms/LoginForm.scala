package forms

import java.util.UUID

object LoginForm {
  import play.api.data._
  import play.api.data.Forms._

  case class LoginData(email: String, password: String, clientId: UUID)

  val form: Form[LoginData] = Form(
    mapping(
      "email" -> text,
      "password" -> text,
      "client_id" -> uuid,
    )(LoginData.apply)(LoginData.unapply)
  )
}
