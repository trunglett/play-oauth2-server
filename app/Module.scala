import com.google.inject.{AbstractModule, TypeLiteral}
import models.User
import services.oauth2provider.{MyDataHandler, MyTokenEndpoint}
import scalaoauth2.provider.{DataHandler, TokenEndpoint}

class Module extends AbstractModule {
  override def configure(): Unit = {
    bind(new TypeLiteral[DataHandler[User]](){})
      .to(classOf[MyDataHandler])

    bind(classOf[TokenEndpoint])
      .to(classOf[MyTokenEndpoint])
  }
}