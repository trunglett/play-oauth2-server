package controllers

import cats.data.EitherT
import javax.inject._
import models.{AuthCode, Client, ClientRepository, User}
import play.api.mvc._
import cats.implicits._
import errors.{BaseError, LoginError}
import forms.LoginForm
import services.authcodeservices.AuthCodeServices
import services.userservices.AuthenticationServices

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class AuthController @Inject()(
                                val controllerComponents: ControllerComponents,
                                authenticationServices: AuthenticationServices,
                                clientRepository: ClientRepository,
                                authCodeServices: AuthCodeServices,
                              )(implicit ec: ExecutionContext) extends BaseController {
  def index(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.auth.index(clientId = request.getQueryString("client_id").getOrElse("")))
  }

  def login(): Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    LoginForm.form.bindFromRequest().fold(
      _ => Future.successful(BadRequest("")),
      loginData => {
        val feAuthCode = (for {
          user <- EitherT(authenticationServices.loginWithCredential(loginData.email, loginData.password))
          client <- EitherT.fromOptionF(clientRepository.getByClientId(loginData.clientId), LoginError(LoginError.REASON_INVALID_CLIENT_ID))
          authCode <- EitherT[Future, BaseError, AuthCode](authCodeServices.create(user.id, Some(client.clientId), client.redirectUri, client.scope))
        } yield authCode).value

        feAuthCode.map(
          _.fold(
            _ => BadRequest(""),
            authCode => Redirect(authCode.redirectUri.get, Map("code" -> Seq(authCode.authorizationCode)))
              .withSession("userId" -> authCode.userId.toString)
          )
        )
      }
    )
  }
}
