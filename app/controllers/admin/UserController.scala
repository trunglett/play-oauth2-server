package controllers.admin

import forms.UserForm
import javax.inject._
import play.api.libs.json.Json
import play.api.mvc._
import services.userservices.RegisterServices

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class UserController @Inject()(
                                val controllerComponents: ControllerComponents,
                                registerServices: RegisterServices,
                              )(implicit ec: ExecutionContext) extends BaseController {
  def create(): Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    UserForm.form.bindFromRequest().fold(
      _ => Future.successful(BadRequest("")),
      userData =>
        registerServices.register(userData.email, userData.password)
          .map(
            _.fold(
              err => BadRequest(Json.toJson(Map("error" -> err.code, "reason" -> err.reason))),
              user => Ok(Json.toJson(user))
            )
          )

    )
  }
}
