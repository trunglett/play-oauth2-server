package controllers.admin

import forms.ClientForm
import javax.inject._
import play.api.libs.json.Json
import play.api.mvc._
import services.clientservices.ClientServices

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ClientController @Inject()(
                                  val controllerComponents: ControllerComponents,
                                  clientServices: ClientServices,
                                )(implicit ec: ExecutionContext) extends BaseController {
  def create(): Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    ClientForm.form.bindFromRequest().fold(
      _ => Future.successful(BadRequest("")),
      clientData =>
        clientServices.create(clientData.redirectUri, clientData.scope)
          .map(
            _.fold(
              err => BadRequest(Json.toJson(Map("error" -> err.code, "reason" -> err.reason))),
              client => Ok(Json.toJson(client))
            )
          )
    )
  }
}
