package controllers

import javax.inject.{Inject, Singleton}
import models.User
import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents}
import scalaoauth2.provider._

import scala.concurrent.ExecutionContext

@Singleton
class OAuth2Controller @Inject() (
                                   val controllerComponents: ControllerComponents,
                                   override val tokenEndpoint: TokenEndpoint,
                                   myDataHandler: DataHandler[User],
                                 ) (implicit ec: ExecutionContext) extends BaseController with OAuth2Provider {
  def accessToken: Action[AnyContent] = Action.async { implicit request =>
    issueAccessToken(myDataHandler)
  }
}