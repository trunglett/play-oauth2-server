package errors

case class CreateAuthTokenError(reason: Int = CreateAuthTokenError.REASON_UNKNOWN) extends BaseError(BaseError.CREATE_AUTH_TOKEN_ERROR)

object CreateAuthTokenError {
  val REASON_UNKNOWN = 0
}