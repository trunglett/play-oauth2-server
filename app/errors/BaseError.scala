package errors

class BaseError(val code: Int)

object BaseError {
  val LOGIN_ERROR = 100100
  val REGISTER_ERROR = 100200
  val CREATE_CLIENT_ERROR = 100300
  val CREATE_AUTH_TOKEN_ERROR = 100400
  val CREATE_AUTH_CODE_ERROR = 100500
}