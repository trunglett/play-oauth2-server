package errors

case class CreateAuthCodeError(reason: Int = CreateAuthTokenError.REASON_UNKNOWN) extends BaseError(BaseError.CREATE_AUTH_CODE_ERROR)

object CreateAuthCodeError {
  val REASON_UNKNOWN = 0
}