package errors

case class CreateClientError(reason: Int = CreateClientError.REASON_UNKNOWN) extends BaseError(BaseError.CREATE_CLIENT_ERROR)

object CreateClientError {
  val REASON_UNKNOWN = 0
}