package errors

case class RegisterError(reason: Int = RegisterError.REASON_UNKNOWN) extends BaseError(BaseError.REGISTER_ERROR)

object RegisterError {
  val REASON_UNKNOWN = 0
}
