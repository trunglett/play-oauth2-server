package errors

case class LoginError(reason: Int = LoginError.REASON_UNKNOWN) extends BaseError(BaseError.LOGIN_ERROR)

object LoginError {
  val REASON_UNKNOWN = 0
  val REASON_INVALID_EMAIL = 1
  val REASON_INVALID_PASSWORD = 2
  val REASON_INVALID_CLIENT_ID = 3
}
