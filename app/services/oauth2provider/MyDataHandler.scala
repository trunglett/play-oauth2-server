package services.oauth2provider

import java.util.UUID

import cats.data.OptionT
import javax.inject.{Inject, Singleton}
import models.{AuthCode, AuthToken, User}
import scalaoauth2.provider._
import services.authcodeservices.AuthCodeServices
import services.authtokenservices.AuthTokenServices
import services.clientservices.ClientServices
import services.userservices.{AuthenticationServices, UserServices}
import cats.implicits._
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class MyDataHandler @Inject()(
                               clientServices: ClientServices,
                               authenticationServices: AuthenticationServices,
                               authTokenServices: AuthTokenServices,
                               authCodeServices: AuthCodeServices,
                               userServices: UserServices,
                             )(implicit ec: ExecutionContext) extends DataHandler[User] {

  def validateClient(maybeClientCredential: Option[ClientCredential], request: AuthorizationRequest): Future[Boolean] =
    maybeClientCredential.fold(Future.successful(false)) {
      cc => clientServices.validateClient(UUID.fromString(cc.clientId))
    }

  def findUser(maybeClientCredential: Option[ClientCredential], request: AuthorizationRequest): Future[Option[User]] =
    request match {
      case pwr: PasswordRequest => authenticationServices.loginWithCredential(pwr.username, pwr.password).map(_.toOption)
      case _ => Future.successful(None)
    }

  def createAccessToken(authInfo: AuthInfo[User]): Future[AccessToken] =
    authTokenServices.create(authInfo.user.id, authInfo.clientId.map(UUID.fromString), authInfo.scope)
      .map(
        _.map(authTokenToAccessToken).getOrElse {
          throw new Throwable
        }
      )

  def getStoredAccessToken(authInfo: AuthInfo[User]): Future[Option[AccessToken]] =
    authTokenServices.getByUserIdAndClientId(authInfo.user.id, authInfo.clientId.map(UUID.fromString))
      .map(_.map(authTokenToAccessToken))

  def refreshAccessToken(authInfo: AuthInfo[User], refreshToken: String): Future[AccessToken] =
    createAccessToken(authInfo) // TODO: delete old authToken

  def findAuthInfoByCode(code: String): Future[Option[AuthInfo[User]]] =
    (for {
      authCode <- OptionT(authCodeServices.getByCode(code))
      authInfo <- OptionT(authCodeToAuthInfo(authCode))
    } yield authInfo).value

  def findAuthInfoByRefreshToken(refreshToken: String): Future[Option[AuthInfo[User]]] =
    (for {
      authToken <- OptionT(authTokenServices.getByRefreshToken(Some(refreshToken)))
      authInfo <- OptionT(authTokenToAuthInfo(authToken))
    } yield authInfo).value

  def deleteAuthCode(code: String): Future[Unit] =
    authCodeServices.deleteByCode(code).map { _ => {} }

  def findAccessToken(token: String): Future[Option[AccessToken]] =
    authTokenServices.getByAccessToken(token).map(_.map(authTokenToAccessToken))

  def findAuthInfoByAccessToken(accessToken: AccessToken): Future[Option[AuthInfo[User]]] =
    (for {
      authToken <- OptionT(authTokenServices.getByAccessToken(accessToken.token))
      authInfo <- OptionT(authTokenToAuthInfo(authToken))
    } yield authInfo).value

  private def authTokenToAccessToken(authToken: AuthToken): AccessToken =
    AccessToken(authToken.accessToken, authToken.refreshToken, authToken.scope, authToken.expiresIn, authToken.createdAt)

  private def authCodeToAuthInfo(authCode: AuthCode) =
    userServices.getById(authCode.userId)
      .map(
        _.map(
          AuthInfo(_, authCode.clientId.map(_.toString), authCode.scope, authCode.redirectUri)
        )
      )

  private def authTokenToAuthInfo(authToken: AuthToken) =
    userServices.getById(authToken.userId)
      .map(
        _.map(
          AuthInfo(_, Some(authToken.clientId.toString), authToken.scope, Some(""))
        )
      )
}
