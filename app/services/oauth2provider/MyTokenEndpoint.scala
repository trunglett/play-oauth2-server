package services.oauth2provider

import scalaoauth2.provider._

class MyTokenEndpoint extends TokenEndpoint {
  override val handlers = Map(
    OAuthGrantType.REFRESH_TOKEN -> new RefreshToken(),
    OAuthGrantType.AUTHORIZATION_CODE -> new AuthorizationCode(),
  )
}
