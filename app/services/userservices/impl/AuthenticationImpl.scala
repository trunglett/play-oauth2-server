package services.userservices.impl

import errors.LoginError
import javax.inject.{Inject, Singleton}
import models.{User, UserRepository}
import org.mindrot.jbcrypt.BCrypt

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class AuthenticationImpl @Inject()(userRepository: UserRepository)(implicit ex: ExecutionContext) extends services.userservices.AuthenticationServices {
  def loginWithCredential(email: String, password: String): Future[Either[LoginError, User]] = {
    userRepository.getByEmail(email)
      .map(
        _.fold[Either[LoginError, User]](Left(LoginError(LoginError.REASON_INVALID_EMAIL)))(checkPassword(_, password))
      )
  }

  private def checkPassword(user: User, password: String): Either[LoginError, User] = {
    Either.cond(BCrypt.checkpw(password, user.password), user, LoginError(LoginError.REASON_INVALID_PASSWORD))
  }
}
