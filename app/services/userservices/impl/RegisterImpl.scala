package services.userservices.impl

import java.util.UUID

import errors.RegisterError
import javax.inject.{Inject, Singleton}
import models.{User, UserRepository}
import org.mindrot.jbcrypt.BCrypt

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class RegisterImpl @Inject()(userRepository: UserRepository)(implicit ec: ExecutionContext) extends services.userservices.RegisterServices {

  def register(email: String, password: String): Future[Either[RegisterError, User]] = {
    val salt = BCrypt.gensalt()
    val hash = BCrypt.hashpw(password, salt)
    val user = User(UUID.randomUUID(), email, hash, Some(salt))
    userRepository.create(user)
      .map(Right(_))
      .recover {
        case _ => Left(RegisterError())
      }
  }
}
