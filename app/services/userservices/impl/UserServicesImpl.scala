package services.userservices.impl

import java.util.UUID

import javax.inject.{Inject, Singleton}
import models.{User, UserRepository}
import services.userservices.UserServices

import scala.concurrent.Future

@Singleton
class UserServicesImpl @Inject()(userRepository: UserRepository) extends UserServices {
  def getById(id: UUID): Future[Option[User]] = userRepository.getById(id)
}
