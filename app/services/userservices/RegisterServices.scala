package services.userservices

import com.google.inject.ImplementedBy
import errors.RegisterError
import models.User

import scala.concurrent.Future

@ImplementedBy(classOf[services.userservices.impl.RegisterImpl])
trait RegisterServices {
  def register(email: String, password: String): Future[Either[RegisterError, User]]
}
