package services.userservices

import com.google.inject.ImplementedBy
import errors.LoginError
import models.User

import scala.concurrent.Future

@ImplementedBy(classOf[services.userservices.impl.AuthenticationImpl])
trait AuthenticationServices {
  def loginWithCredential(email: String, password: String): Future[Either[LoginError, User]]
}
