package services.userservices

import java.util.UUID

import com.google.inject.ImplementedBy
import models.User
import services.userservices.impl.UserServicesImpl

import scala.concurrent.Future

@ImplementedBy(classOf[UserServicesImpl])
trait UserServices {
  def getById(id: UUID): Future[Option[User]]
}
