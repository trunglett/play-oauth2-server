package services.clientservices

import java.util.UUID

import com.google.inject.ImplementedBy
import errors.CreateClientError
import models.Client

import scala.concurrent.Future

@ImplementedBy(classOf[services.clientservices.impl.ClientServicesImpl])
trait ClientServices {
  def create(redirectUri: Option[String], scope: Option[String]): Future[Either[CreateClientError, Client]]
  def validateClient(clientId: UUID): Future[Boolean]
}
