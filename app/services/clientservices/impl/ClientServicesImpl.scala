package services.clientservices.impl

import java.util.UUID

import errors.CreateClientError
import helpers.Generator
import javax.inject.{Inject, Singleton}
import models.{Client, ClientRepository}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ClientServicesImpl @Inject()(clientRepository: ClientRepository)(implicit ec: ExecutionContext) extends services.clientservices.ClientServices {
  def create(redirectUri: Option[String], scope: Option[String]): Future[Either[CreateClientError, Client]] = {
    val clientId = UUID.randomUUID()
    val clientSecret = Some(Generator.secret)
    val client = Client(clientId, clientSecret, redirectUri, scope)
    clientRepository.create(client)
      .map(Right(_))
      .recover {
        case _ => Left(CreateClientError())
      }
  }

  def validateClient(clientId: UUID): Future[Boolean] = {
    clientRepository.getByClientId(clientId).map(_.isDefined)
  }
}
