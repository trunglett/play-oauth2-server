package services.authtokenservices

import java.util.UUID

import com.google.inject.ImplementedBy
import errors.CreateAuthTokenError
import models.AuthToken
import services.authtokenservices.impl.AuthTokenServicesImpl

import scala.concurrent.Future

@ImplementedBy(classOf[AuthTokenServicesImpl])
trait AuthTokenServices {
  def create(userId: UUID, clientId: Option[UUID], scope: Option[String], expiresIn: Option[Long] = Some(3600L)): Future[Either[CreateAuthTokenError, AuthToken]]
  def getByUserIdAndClientId(userId: UUID, clientId: Option[UUID]): Future[Option[AuthToken]]
  def getByAccessToken(accessToken: String): Future[Option[AuthToken]]
  def getByRefreshToken(refreshToken: Option[String]): Future[Option[AuthToken]]
}
