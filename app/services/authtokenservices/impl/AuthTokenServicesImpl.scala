package services.authtokenservices.impl

import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.UUID

import errors.CreateAuthTokenError
import helpers.Generator
import javax.inject.{Inject, Singleton}
import models.{AuthToken, AuthTokenRepository}
import services.authtokenservices.AuthTokenServices

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class AuthTokenServicesImpl @Inject()(authTokenRepository: AuthTokenRepository)(implicit ec: ExecutionContext) extends AuthTokenServices {
  def create(userId: UUID, clientId: Option[UUID], scope: Option[String], expiresIn: Option[Long] = Some(3600L)): Future[Either[CreateAuthTokenError, AuthToken]] = {
    val accessToken = Generator.token
    val refreshToken = Some(Generator.token)
    val createdAt = Timestamp.valueOf(LocalDateTime.now())
    val authToken = AuthToken(userId, accessToken, refreshToken, expiresIn, createdAt, clientId, scope)
    authTokenRepository.create(authToken)
      .map(Right(_))
      .recover {
        case _ => Left(CreateAuthTokenError())
      }
  }

  def getByUserIdAndClientId(userId: UUID, clientId: Option[UUID]): Future[Option[AuthToken]] = authTokenRepository.getByUserIdAndClientId(userId, clientId)
  def getByAccessToken(accessToken: String): Future[Option[AuthToken]] = authTokenRepository.getByAccessToken(accessToken)
  def getByRefreshToken(refreshToken: Option[String]): Future[Option[AuthToken]] = authTokenRepository.getByRefreshToken(refreshToken)
}
