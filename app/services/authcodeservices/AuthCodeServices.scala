package services.authcodeservices

import java.util.UUID

import com.google.inject.ImplementedBy
import errors.CreateAuthCodeError
import models.AuthCode
import services.authcodeservices.impl.AuthCodeServicesImpl

import scala.concurrent.Future

@ImplementedBy(classOf[AuthCodeServicesImpl])
trait AuthCodeServices {
  def create(userId: UUID, clientId: Option[UUID], redirectUri: Option[String], scope: Option[String], expiresIn: Long = 300L): Future[Either[CreateAuthCodeError, AuthCode]]
  def getByCode(code: String): Future[Option[AuthCode]]
  def getByUserIdAndClientId(userId: UUID, clientId: Option[UUID]): Future[Option[AuthCode]]
  def deleteByCode(code: String): Future[Int]
}
