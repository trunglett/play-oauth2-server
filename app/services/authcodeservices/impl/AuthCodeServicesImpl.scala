package services.authcodeservices.impl

import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.UUID

import errors.CreateAuthCodeError
import helpers.Generator
import javax.inject.{Inject, Singleton}
import models.{AuthCode, AuthCodeRepository}
import services.authcodeservices.AuthCodeServices

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class AuthCodeServicesImpl @Inject()(authCodeRepository: AuthCodeRepository)(implicit ec: ExecutionContext) extends AuthCodeServices {
  def create(userId: UUID, clientId: Option[UUID], redirectUri: Option[String], scope: Option[String], expiresIn: Long = 300L): Future[Either[CreateAuthCodeError, AuthCode]] = {
    val code = Generator.token
    val createdAt = Timestamp.valueOf(LocalDateTime.now())
    val authCode = AuthCode(userId, code, expiresIn, createdAt, redirectUri, clientId, scope)
    authCodeRepository.create(authCode)
      .map(Right(_))
      .recover {
        case _ => Left(CreateAuthCodeError())
      }
  }
  def getByCode(code: String): Future[Option[AuthCode]] = authCodeRepository.getByCode(code)
  def getByUserIdAndClientId(userId: UUID, clientId: Option[UUID]): Future[Option[AuthCode]] = authCodeRepository.getByUserIdAndClientId(userId, clientId)
  def deleteByCode(code: String): Future[Int] = authCodeRepository.deleteByCode(code)
}
