package models

import java.sql.Timestamp
import java.util.UUID
import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class AuthTokenRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {
  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class AuthTokenTable(tag: Tag) extends Table[AuthToken](tag, "auth_tokens") {

    def userId = column[UUID]("user_id")

    def accessToken = column[String]("access_token", O.PrimaryKey)

    def refreshToken = column[Option[String]]("refresh_token")

    def scope = column[Option[String]]("scope")

    def expiresIn = column[Option[Long]]("expires_in")

    def createdAt = column[Timestamp]("created_at")

    def clientId = column[Option[UUID]]("client_id")

    def * = (userId, accessToken, refreshToken, expiresIn, createdAt, clientId, scope) <> ((AuthToken.apply _).tupled, AuthToken.unapply)
  }

  val authTokens = TableQuery[AuthTokenTable]

  def createDBIO(authToken: AuthToken): DBIO[AuthToken] = (authTokens += authToken).map(_ => authToken)

  def create(authToken: AuthToken): Future[AuthToken] = db.run {
    createDBIO(authToken)
  }

  def getByRefreshToken(refreshToken: Option[String]): Future[Option[AuthToken]] = db.run {
    authTokens.filter(_.refreshToken === refreshToken).result.headOption
  }

  def getByUserIdAndClientId(userId: UUID, clientId: Option[UUID]): Future[Option[AuthToken]] = db.run {
    authTokens.filter(_.userId === userId).filter(_.clientId === clientId).result.headOption
  }

  def getByAccessToken(accessToken: String): Future[Option[AuthToken]] = db.run {
    authTokens.filter(_.accessToken === accessToken).result.headOption
  }
}