package models

import java.sql.Timestamp
import java.util.UUID

case class AuthToken(
                        userId: UUID,
                        accessToken: String,
                        refreshToken: Option[String],
                        expiresIn: Option[Long],
                        createdAt: Timestamp,
                        clientId: Option[UUID],
                        scope: Option[String],
                      )

