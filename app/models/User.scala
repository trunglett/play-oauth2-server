package models

import java.sql.Timestamp
import java.util.UUID

import play.api.libs.json.{Format, Json}

case class User(
                 id: UUID,
                 email: String,
                 password: String,
                 salt: Option[String],
               )

object User {
  implicit val timestampFormat: Format[Timestamp] = helpers.JsonFormatters.timestampFormat
  implicit val userFormat: Format[User] = Json.format[User]
}