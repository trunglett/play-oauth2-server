package models

import java.util.UUID

import javax.inject.{Inject, Singleton}
import org.mindrot.jbcrypt.BCrypt
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class UserRepository @Inject() (dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {
  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class UserTable(tag: Tag) extends Table[User](tag, "users") {

    def id = column[UUID]("id", O.PrimaryKey)
    def email = column[String]("email")
    def password = column[String]("password")
    def salt = column[Option[String]]("salt")
    def * = (id, email, password, salt) <> ((User.apply _).tupled, User.unapply)
  }

  val users = TableQuery[UserTable]

  def createDBIO(user: User): DBIO[User] = (users += user).map(_ => user)

  def create(user: User): Future[User] = db.run {
    createDBIO(user)
  }

  def getById(id: UUID): Future[Option[User]] = db.run {
    users.filter(_.id === id).result.headOption
  }

  def getByEmail(email: String): Future[Option[User]] = db.run {
    users.filter(_.email === email).result.headOption
  }
}