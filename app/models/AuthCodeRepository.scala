package models

import java.sql.Timestamp
import java.util.UUID

import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class AuthCodeRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {
  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class AuthCodeTable(tag: Tag) extends Table[AuthCode](tag, "auth_codes") {

    def userId = column[UUID]("user_id")

    def authorizationCode = column[String]("code", O.PrimaryKey)

    def redirectUri = column[Option[String]]("redirect_uri")

    def scope = column[Option[String]]("scope")

    def createdAt = column[Timestamp]("created_at")

    def expiresIn = column[Long]("expires_in")

    def clientId = column[Option[UUID]]("client_id")

    def * = (userId, authorizationCode, expiresIn, createdAt, redirectUri, clientId, scope) <> ((AuthCode.apply _).tupled, AuthCode.unapply)
  }

  val authCodes = TableQuery[AuthCodeTable]

  def create(authCode: AuthCode): Future[AuthCode] = db.run {
    authCodes += authCode
  }.map { _ => authCode }

  def getByCode(code: String): Future[Option[AuthCode]] = db.run {
    authCodes.filter(_.authorizationCode === code).result.headOption
  }

  def getByUserIdAndClientId(userId: UUID, clientId: Option[UUID]): Future[Option[AuthCode]] = db.run {
    authCodes.filter(_.userId === userId).filter(_.clientId === clientId).result.headOption
  }

  def deleteByCode(code: String): Future[Int] = db.run {
    authCodes.filter(_.authorizationCode === code).delete
  }
}