package models

import java.util.UUID

import helpers.Generator
import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ClientRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {
  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class ClientTable(tag: Tag) extends Table[Client](tag, "clients") {

    def clientId = column[UUID]("client_id", O.PrimaryKey)
    def clientSecret = column[Option[String]]("client_secret")
    def redirectUri = column[Option[String]]("redirect_uri")
    def scope = column[Option[String]]("scope")
    def * = (clientId, clientSecret, redirectUri, scope) <> ((Client.apply _).tupled, Client.unapply)
  }

  val clients = TableQuery[ClientTable]

  def createDBIO(client: Client): DBIO[Client] = (clients += client).map(_ => client)

  def create(client: Client): Future[Client] = db.run {
    createDBIO(client)
  }

  def getByClientId(clientId: UUID): Future[Option[Client]] = db.run {
    clients.filter(_.clientId === clientId).result.headOption
  }
}