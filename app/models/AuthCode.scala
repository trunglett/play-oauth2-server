package models

import java.sql.Timestamp
import java.util.UUID

case class AuthCode(
                     userId: UUID,
                     authorizationCode: String,
                     expiresIn: Long,
                     createdAt: Timestamp,
                     redirectUri: Option[String],
                     clientId: Option[UUID],
                     scope: Option[String],
                   )
