package models

import java.util.UUID

import play.api.libs.json.{Format, Json}

case class Client(
                   clientId: UUID,
                   clientSecret: Option[String],
                   redirectUri: Option[String],
                   scope: Option[String]
                 )

object Client {
  implicit val clientFormat: Format[Client] = Json.format[Client]
}